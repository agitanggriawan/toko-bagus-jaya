$(document).ready(function () {
  let nomor = 1;
  tambahBarang = () => {
    const added = `
      <div class="classTable" id="childTable_${nomor}">
        <div class="row">
          <div class="col s6">
            <div class="input-field">
              <input id="namaBarang_${nomor}" type="text" placeholder="Nama Barang">
            </div>
          </div>
          <div class="col s1">
            <div class="input-field">
              <input id="qty_${nomor}" type="number" placeholder="Qty" value="">
            </div>
          </div>
          <div class="col s2">
            <div class="input-field">
              <input id="harga_${nomor}" type="number" placeholder="Harga" value="">
            </div>
          </div>
          <div class="col s2">
            <div class="input-field">
              <input id="total_${nomor}" type="text" placeholder="Sub Total" onfocus="countTotal('qty_${nomor}', 'harga_${nomor}', 'total_${nomor}')"">
            </div>
          </div>
          <div class="col s1">
            <div class="input-field">
            <button class="waves-effect waves-dark btn btn-sm red" type="button" onclick="deleteTable('childTable_${nomor}')">Hapus</button>
          </div>
        </div>
      </div>
    `;
    $('.parentTable').append(added);
    nomor++;
  };

  deleteTable = (id) => {
    $(`#${id}`).remove();
  };

  countTotal = (qty, harga, total) => {
    let sum = $(`#${qty}`).val() * $(`#${harga}`).val();
    $(`#${total}`).val(`Rp. ${sum.toLocaleString()}`);
    $(`#${total}`).prop('readonly', true);
  };

  grandTotal = (grandTotal) => {
    const itemBarang = $('.parentTable').find('.classTable');
    const valDiscount = Number($('#discount').val());
    let total = 0;
    let sum = 0;
    debugger;
    for (let i = 0; i < itemBarang.length; i++) {
      let inputBarang = $(itemBarang[i]).find('input');
      sum += $(inputBarang[1]).val() * $(inputBarang[2]).val();
    }
    total = sum;
    if (valDiscount !== 0) {
      const totalDiscount = (total = sum * (valDiscount / 100));
      total = sum - totalDiscount;
    }

    $(`#${grandTotal}`).val(`Rp. ${total.toLocaleString()}`);
    $(`#${grandTotal}`).prop('readonly', true);
  };

  totalBayar = (grandTotal, totalBayar) => {
    var gt = Number(
      $(`#${grandTotal}`)
        .val()
        .replace(/[^0-9]/g, '')
    );

    if (gt > Number($(`#${totalBayar}`).val())) {
      M.toast({
        html: 'Total bayar kurang!',
        classes: 'rounded',
      });
      $('#saveAll').prop('disabled', true);
    } else {
      $('#saveAll').prop('disabled', false);
    }
  };

  saveAll = () => {
    const itemBarang = $('.parentTable').find('.classTable');
    let total = 0;
    let sum = 0;
    let totalBayar = $('#totalBayar').val();
    const valDiscount = Number($('#discount').val());
    let array = [];

    for (let i = 0; i < itemBarang.length; i++) {
      let inputBarang = $(itemBarang[i]).find('input');
      let item = {
        namaBarang: $(inputBarang[0]).val(),
        qty: $(inputBarang[1]).val(),
        harga: $(inputBarang[2]).val(),
        subTotal: $(inputBarang[1]).val() * $(inputBarang[2]).val(),
      };
      sum += $(inputBarang[1]).val() * $(inputBarang[2]).val();
      array.push(item);
    }

    total = sum;
    if (valDiscount !== 0) {
      const totalDiscount = (total = sum * (valDiscount / 100));
      total = sum - totalDiscount;
    }

    $('#grandTotal').val(`Rp. ${total.toLocaleString()}`);
    $('#grandTotal').prop('readonly', true);

    const body = {
      items: array,
      total: sum,
      grandTotal: total,
      totalBayar,
      discount: valDiscount,
    };
    let sentData = new FormData();
    sentData.append('body', JSON.stringify(body));
    try {
      $.ajax({
        type: 'POST',
        url: './config/commitTransaction.php',
        data: sentData,
        contentType: false,
        processData: false,
        cache: false,
        async: false,
        success: (data) => {
          $('button').prop('disabled', true);
          const decode = JSON.parse(data);
          setTimeout(() => {
            window.open(
              `config/invoice-pdf.php?id=${window.btoa(decode.id)}`,
              `_blank`
            );
          }, 1000);
          M.toast({
            html: 'Transaksi berhasil disimpan',
            completeCallback: () => {
              window.location = './home.php';
            },
            classes: 'rounded',
          });
        },
        error: (error) => {
          console.log('Error', error);
        },
      });
    } catch (error) {
      console.log('Error =>', error);
    }
  };
});
