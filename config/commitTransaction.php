<?php
  include "./config.php";

  $res = new \stdClass();
  $body = json_decode($_POST['body']);
  $items = $body->items;
  $total = $body->total;
  $grandTotal = $body->grandTotal;
  $totalBayar = $body->totalBayar;
  $kembalian = $totalBayar - $grandTotal;
  $diskon = $body->discount;

  $insertTransaksi = mysqli_query($connect, "INSERT INTO transaksi(items, grandTotal, totalBayar, kembalian, diskon, total) VALUES('" .json_encode($items). "', $grandTotal, $totalBayar, $kembalian, $diskon, $total)");

  if ($insertTransaksi) {
    $res->code = "OK";
    $res->msg = "Success";
    $res->id = mysqli_insert_id($connect);
  } else {
    $res->code = "FAIL";
    $res->msg = mysqli_error($connect);
  }

  echo json_encode($res);
?>