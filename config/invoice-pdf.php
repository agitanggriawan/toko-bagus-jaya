<?php
ob_start();
require_once '../assets/dompdf/lib/html5lib/Parser.php';
require_once '../assets/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once '../assets/dompdf/lib/php-svg-lib/src/autoload.php';
require_once '../assets/dompdf/src/Autoloader.php';
require("invoice.php");
Dompdf\Autoloader::register();

use Dompdf\Dompdf;
$dompdf = new Dompdf();
$dompdf->load_html(ob_get_clean());
$page_count = $dompdf->get_canvas()->get_page_number();

$customPaper = array(0, 0, 147.40, 209.76 * $page_count + 100);
$dompdf->setPaper($customPaper, 'portrait');

$dompdf->render();
$dompdf->stream("INVOICE.pdf", array("Attachment" => false));
?>