<?php
  require './config.php';
  $username = mysqli_real_escape_string($connect, $_POST['username']);
  $password = mysqli_real_escape_string($connect, $_POST['password']);

  $User = $connect->prepare("SELECT id, username, nama FROM pengguna WHERE username = ? and password = ?");
  $User->bind_param('ss', $username, $password);
  $User->execute();
  $User->store_result();
  $User->bind_result($id, $username, $nama);

  if ($User->num_rows > 0) {
    session_start();
    $User->fetch();
    $_SESSION['id'] = base64_encode($id);
    $_SESSION['username'] = base64_encode($username);
    $_SESSION['nama'] = base64_encode($nama);
    $User->close();
    header('Location: ../home');
  } else {
    header('Location: ../index?n=f1');
  }
?>