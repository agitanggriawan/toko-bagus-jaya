<?php
	require "config.php";
	$query = mysqli_query($connect, "SELECT * FROM transaksi WHERE id = ".base64_decode($_GET['id'])." ");
	$data = mysqli_fetch_array($query);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Invoice Penjualan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="main.css">
	<style>
		html {
			margin: 0 0 0 10
		},
		body {
			font-size: 7pt
		}
	</style>
</head>
<body>
	<p style="text-align: center;">================================================<br/>Toko Bagus Jaya<br/>================================================<</p>
	<table width="100%">
		<tr>
			<td>Barang</td>
			<td>Harga</td>
			<td>Qty</td>
			<td>Sub Total</td>
		</tr>
		<?php
		$array = json_decode($data['items']);
		$index = 1; $total = 0;
		foreach($array as $items){
			echo '<tr>';
			echo '<td>'.$items->namaBarang.'</td>';
			echo '<td>Rp. '.number_format($items->harga).'</td>';
			echo '<td>'.$items->qty.'</td>';
			echo '<td>'.$items->subTotal.'</td>';
			echo '</tr>';
		$index++; $total += 1;
		}
		?>
		<tr>
			<td colspan="4">-----------------------------------------------------------------------------</td>
		</tr>
		<tr>
			<td>Total Item: <?php echo $total ?></td>
			<td colspan="2" style="text-align: right">Total: </td>
			<td><?php echo number_format($data['total']) ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: right">Diskon: </td>
			<td><?php echo number_format($data['total'] * $data['diskon'] / 100) ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: right">Grand Total: </td>
			<td><?php echo number_format($data['grandTotal']) ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: right">Tunai: </td>
			<td><?php echo number_format($data['totalBayar']) ?></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: right">Kembalian: </td>
			<td><?php echo number_format($data['kembalian']) ?></td>
		</tr>
		<tr>
		</tr>
	</table>
</body>
</html>