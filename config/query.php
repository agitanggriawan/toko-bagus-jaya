<?php
  include "./config.php";

  if ($_GET['name'] === "histories") {
    $query = mysqli_query($connect, "SELECT * FROM transaksi ORDER BY 1 DESC");
    $arr = [];
    $i = 0;
    while($data = mysqli_fetch_array($query)){
      $arr[$i]['no'] = $i + 1 . '.';
      $arr[$i]['createdAt'] = $data[5];
      $arr[$i]['totalBayar'] = 'Rp. '.number_format($data[4]);
      $arr[$i]['kembalian'] = 'Rp. '.number_format($data[3]);
      $arr[$i]['grandTotal'] = 'Rp. '.number_format($data[2]);
      $arr[$i]['total'] = 'Rp. '.number_format($data[7]);
      $arr[$i]['diskon'] = 'Rp. '.number_format($data[7] - $data[2]);
      $arr[$i]['action'] = $data[0];
      $i++;
    }
    header('Content-Type: application/json');
    echo json_encode($arr);
  }
?>