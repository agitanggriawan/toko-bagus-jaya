<?php
  session_start();
  include './config/config.php';
  if (!$_SESSION['id']) {
    header('Location: index');
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
  <title>Toko Bagus Jaya</title>
  <link href="dist/css/style.css" rel="stylesheet">
  <link href="dist/css/pages/data-table.css" rel="stylesheet">
  <link href="assets/extra-libs/prism/prism.css" rel="stylesheet">
</head>

<body>
  <div class="main-wrapper" id="main-wrapper">
    <div class="preloader">
      <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Toko Bagus Jaya</p>
      </div>
    </div>
    <header class="topbar">
      <nav>
        <div class="nav-wrapper">
          <a href="javascript:void(0)" class="brand-logo">
            <span class="icon">
              <img class="light-logo" src="assets/images/logo-light-icon.png">
              <img class="dark-logo" src="assets/images/logo-icon.png">
            </span>
            <span class="text">
              <img class="light-logo" src="assets/images/logo-light-text.png">
              <img class="dark-logo" src="assets/images/logo-text.png">
            </span>
          </a>
          <ul class="left">
            <li class="hide-on-med-and-down">
              <a href="javascript: void(0);" class="nav-toggle">
                <span class="bars bar1"></span>
                <span class="bars bar2"></span>
                <span class="bars bar3"></span>
              </a>
            </li>
            <li class="hide-on-large-only">
              <a href="javascript: void(0);" class="sidebar-toggle">
                <span class="bars bar1"></span>
                <span class="bars bar2"></span>
                <span class="bars bar3"></span>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <aside class="left-sidebar">
      <ul id="slide-out" class="sidenav">
        <li>
          <ul class="collapsible">
          <li class="small-cap"><span class="hide-menu">Transaksi</span></li>
            <li>
                <a href="home" class="collapsible-header"><i class="material-icons">perm_contact_calendar</i><span class="hide-menu"> Transaksi </span></a>
            </li>
            <li>
                <a href="./config/logout" class="collapsible-header"><i class="material-icons">lock</i><span class="hide-menu"> Logout  </span></a>
            </li>
          </ul>
        </li>
      </ul>
    </aside>
    <div class="page-wrapper">
      <div class="page-titles">
        <div class="d-flex align-items-center">
          <h5 class="font-medium m-b-0">Toko Bagus Jaya</h5>
        </div>
      </div>
      <?php
      if (isset($_GET['detail'])) {
      $query = mysqli_query($connect, "SELECT * FROM transaksi WHERE id = ".base64_decode($_GET['detail'])." ");
	    $data = mysqli_fetch_array($query);
      ?>
      <div class="containter-fluid">
        <div class="row">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
                <div class="row">
                  <div class="col s12">
                    <h5 class="card-title">Detail Transaksi Penjualan</h5>
                    <p style="margin-top: 50px">ID Transaksi: #<?php echo '<b>'.$data['id'].'</b>' ?></p>
                    <p>Tanggal Transaksi: <?php echo '<b>'.$data['createdAt'].'</b>' ?></p>
                    <table class="responsive-table">
                      <tr>
                        <td width="35px">No.</td>
                        <td>Nama Barang</td>
                        <td>Harga</td>
                        <td>Qty</td>
                        <td>Sub Total</td>
                      </tr>
                      <?php
                      $array = json_decode($data['items']);
                      $index = 1;
                      foreach($array as $items){
                        echo '<tr>';
                        echo '<td>'.$index.'.</td>';
                        echo '<td>'.$items->namaBarang.'</td>';
                        echo '<td>Rp. '.number_format($items->harga).'</td>';
                        echo '<td>'.$items->qty.'</td>';
                        echo '<td>'.$items->subTotal.'</td>';
                        echo '</tr>';
                      $index++;
                      }
                      ?>
                      <tr>
                        <td colspan="4" style="text-align: right">Total: </td>
                        <td><?php echo 'Rp. '.number_format($data['total']) ?></td>
                      </tr>
                      <tr>
                        <td colspan="4" style="text-align: right">Diskon: </td>
                        <td><?php echo 'Rp. '.number_format($data['total'] * $data['diskon'] / 100) ?></td>
                      </tr>
                      <tr>
                        <td colspan="4" style="text-align: right">Grand Total: </td>
                        <td><?php echo 'Rp. '.number_format($data['grandTotal']) ?></td>
                      </tr>
                      <tr>
                        <td colspan="4" style="text-align: right">Total Bayar: </td>
                        <td><?php echo 'Rp. '.number_format($data['totalBayar']) ?></td>
                      </tr>
                      <tr>
                        <td colspan="4" style="text-align: right">Kembalian: </td>
                        <td><?php echo 'Rp. '.number_format($data['kembalian']) ?></td>
                      </tr>
                      <tr>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="row" style="margin-top: 25px">
                  <div class="col s12">
                    <a href="home" class="btn btn-large waves-effect waves-light green" title="Kembali"><i class="material-icons">chevron_left</i></a>
                    <a href="./config/invoice-pdf?id=<?php echo $_GET['detail'] ?>" class="btn btn-large waves-effect waves-light blue" target="_blank" title="Cetak"><i class="material-icons">print</i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
      } else {
      ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col s12">
            <ul class="tabs">
                <li class="tab col s6"><a href="#transaksi" class="active">Transaksi</a></li>
                <?php if ($_SESSION['username'] == base64_encode('admin')) { ?>
                <li class="tab col s6"><a href="#history">History</a></li>
                <?php } ?>
            </ul>
          </div>
            <div id="transaksi" class="col s12">
              <div class="row">
                <div class="col s12">
                  <div class="card">
                    <div class="card-content">
                      <h5 class="card-title">Transaksi Penjualan</h5>
                      <div class="row">
                        <div class="col s6">
                          <div class="input-field">Nama Barang</div>
                        </div>
                        <div class="col s1">
                          <div class="input-field">Qty</div>
                        </div>
                        <div class="col s2">
                          <div class="input-field">Harga</div>
                        </div>
                        <div class="col s2">
                          <div class="input-field">Sub Total</div>
                        </div>
                      </div>
                      <div class="parentTable">
                        <div class="row classTable" id="childTable">
                          <div class="col s6">
                            <div class="input-field">
                              <input id="namaBarang" type="text" placeholder="Nama Barang" autofocus>
                            </div>
                          </div>
                          <div class="col s1">
                            <div class="input-field">
                              <input id="qty" type="number" placeholder="Qty" value="">
                            </div>
                          </div>
                          <div class="col s2">
                            <div class="input-field">
                              <input id="harga" type="number" placeholder="Harga" value="">
                            </div>
                          </div>
                          <div class="col s3">
                            <div class="input-field">
                              <input id="total" type="text" placeholder="Sub Total" onfocus="countTotal('qty', 'harga', 'total')">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col s2 offset-s7">
                          <div class="input-field right" style="margin-top: 30px">Diskon</div>
                        </div>
                        <div class="col s2">
                          <div class="input-field">
                            <input id="discount" type="number" value="0" min="0" max="100" placeholder="Diskon">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s7">
                          <button id="addButton" class="btn green waves-effect waves-light left" type="button" onclick="tambahBarang()">Tambah</button>
                        </div>
                        <div class="col s2">
                          <div class="input-field right" style="margin-top: 30px">Grand Total</div>
                        </div>
                        <div class="col s2">
                          <div class="input-field">
                            <input id="grandTotal" type="text" placeholder="Grand Total" onfocus="grandTotal('grandTotal')" readonly>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col s2 offset-s7">
                          <div class="input-field right" style="margin-top: 30px">Total Bayar</div>
                        </div>
                        <div class="col s2">
                          <div class="input-field">
                            <input id="totalBayar" type="number" placeholder="Total Bayar" onblur="totalBayar('grandTotal', 'totalBayar')">
                          </div>
                        </div>
                        <div class="input-field col s1">
                          <button id="saveAll" class="btn blue waves-effect waves-light right" disabled type="button" onclick="saveAll()">Simpan</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php if ($_SESSION['username'] == base64_encode('admin')) { ?>
            <div id="history" class="col s12">
              <div class="row">
                <div class="col s12">
                  <div class="card">
                    <div class="card-content">
                      <table id="tableHistories" class="table table-striped table-bordered display" style="width:100%">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Tanggal Transaksi</th>
                              <th>Total Bayar</th>
                              <th>Grand Total</th>
                              <th>Total</th>
                              <th>Diskon</th>
                              <th>Kembalian</th>
                              <th width="150px">Aksi</th>
                            </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
        </div>
      </div>
      <?php
      }
      ?>
      <!-- ============================================================== -->
      <!-- Container fluid scss in scafholding.scss -->
      <!-- ============================================================== -->
      <footer class="center-align m-b-30">All Rights Reserved by Bagus Jaya. Designed and Developed by <a href="#">BagusJaya</a>.</footer>
    </div>
  </div>
  <script src="assets/libs/jquery/dist/jquery.min.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script src="assets/libs/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
  <script src="dist/js/app.js"></script>
  <script src="dist/js/app.init.mini-sidebar.js"></script>
  <script src="dist/js/app-style-switcher.js"></script>
  <script src="dist/js/custom.min.js"></script>
  <script src="dist/js/index.js"></script>
  <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
  <script src="dist/js/pages/datatable/datatable-advanced.init.js"></script>
  <script src="assets/extra-libs/prism/prism.js"></script>
  <script>
    $('#tableHistories').DataTable({
      "processing": true,
      "ajax": {
      "url" : "./config/query.php?name=histories",
      "type": "GET",
      "dataSrc": ""
      },
      "columns" : [
        { "data": "no" },
        { "data": "createdAt" },
        { "data": "totalBayar" },
        { "data": "grandTotal" },
        { "data": "total" },
        { "data": "diskon" },
        { "data": "kembalian" },
        {
          "mData" : "action",
          "mRender" : function(data, type, row){
          return `
            <a href="home?detail=${window.btoa(row['action'])}" class="btn-floating btn-large waves-effect waves-light green" title="Detail"><i class="material-icons">list</i></a>
            <a href="./config/invoice-pdf?id=${window.btoa(row['action'])}" class="btn-floating btn-large waves-effect waves-light blue" target="_blank" title="Cetak"><i class="material-icons">print</i></a>
          `;
          }
        }
      ]
    });
  </script>
</body>
</html>