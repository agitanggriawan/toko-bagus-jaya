<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
  <title>Aplikasi Kasir Toko Bagus</title>
  <link href="dist/css/style.css" rel="stylesheet">
  <link href="dist/css/authentication.css" rel="stylesheet">
</head>

<body>
  <div class="main-wrapper">
    <div class="preloader">
      <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Menunggu</p>
      </div>
    </div>
    <?php
    if (isset($_GET['n'])){
      if($_GET['n'] == "f1") {
          echo '
          <script>
            setTimeout(function(){
              M.toast({html: "Username atau Password salah", classes: "rounded"});
            },1000)
          </script>
          ';
      }
    }
    ?>
    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(assets/images/auth-bg.jpg) no-repeat center center;">
      <div class="auth-box">
        <div id="loginform">
          <div class="logo">
            <span class="db"><img src="assets/images/logo-icon.png" alt="logo" /></span>
            <h5 class="font-medium m-b-20">Halaman Login</h5>
          </div>
          <div class="row">
            <form class="col s12" method="POST" action="./config/verify.php">
              <div class="row">
                <div class="input-field col s12">
                  <input id="username" name="username" type="text" class="validate" required>
                  <label for="username">Username</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="password" name="password" type="password" class="validate" required>
                  <label for="password">Password</label>
                </div>
              </div>
              <div class="row m-t-40">
                <div class="col s12">
                  <button class="btn-large w100 blue accent-4" type="submit">Login</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="assets/libs/jquery/dist/jquery.min.js"></script>
  <script src="dist/js/materialize.min.js"></script>
  <script>
  $('.tooltipped').tooltip();
  $('#to-recover').on("click", function() {
    $("#loginform").slideUp();
    $("#recoverform").fadeIn();
  });
  $(function() {
    $(".preloader").fadeOut();
  });
  </script>
</body>
</html>